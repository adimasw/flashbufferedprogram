`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:59:33 04/29/2018 
// Design Name: 
// Module Name:    FlashBufferedProgram_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FlashBufferedProgram_top(input system_clk_p,
	 input system_clk_n,
    output bpi_we_n,
    output bpi_ce_n,
    output bpi_oe_n,
    output [25:0] bpi_addr_cmd,
	 output bpi_adv,
	 output led_0,
	 output led_1,
	 output led_2,
	 output led_3,
    inout [15:0] bpi_data
    );
	 
clockgen clkgen(.CLK_IN1_P(system_clk_p), .CLK_IN1_N(system_clk_n),.CLK_OUT1(clk));	 
reg[3:0] status;

reg[15:0] data=0;
reg we_n=1;
reg ce_n=0;
reg oe_n=1;
reg adv=0;
reg led=0;

reg[25:0] addr=26'h010000;
reg[3:0] state=0;
reg [8:0] count_to_512;
assign bpi_addr_cmd=addr;
assign bpi_we_n=we_n;
assign bpi_oe_n=oe_n;
assign bpi_ce_n=ce_n;
assign bpi_adv=adv;
assign bpi_data=data;

reg unlock=0;
assign led_0=status[0];
assign led_1=status[1];
assign led_2=status[2];
assign led_3=status[3];

always @(posedge clk)
begin
if (unlock==1)
	begin
		case (state)
			4'b0:	
			begin
				we_n<=0;
				adv<=1;
				ce_n<=0;
				data<=16'hE8;
				state<=4'b1;
			end
			4'h1:
			begin
				we_n<=1;
				adv<=0;
				oe_n<=0;
				state<=4'h2;
				data<=16'hz;
			end
			4'h2:
			begin
				state<=4'hb;
			end
			4'h3:
			begin
			oe_n<=1;
				if (bpi_data[7]==1)
				begin
					state<=4'h4;
				end
				else
				begin
					state<=4'h1;
				end
			end
			4'h4:
			begin
				we_n<=0;
				data<=16'd15;
				state<=4'h5;
			end
			4'h5:
			begin	
				we_n<=1;
				state<=4'hd;
			end
			4'h6:
			begin
				we_n<=1;
				adv<=0;
				oe_n<=0;
				state<=4'h7;
				data<=16'hz;
			end
			4'h7:
				state<=4'hc;
			4'h8:
			begin
				oe_n<=1;
				status<=bpi_data[7:4];
				if (bpi_data[7] ==1)
				begin
					state<=4'h9;
				end
				else
				begin
					state<=4'h6;
				end
			end
			4'h9:
			begin
				led<=1;
				ce_n<=1;
			end
			4'ha:
			begin
				we_n<=0;
				adv<=1;
				state<=4'h6;
			end
			4'hb:
				state<=4'h3;
			4'hc:
				state<=4'h8;
			4'hd:
			begin
				we_n<=0;
				state<=4'he;
			end
			4'he:
			begin
				if (addr==26'h10000+26'd15)
				begin
					state<=4'ha;
					data<=16'hD0;
					addr<={addr[25:9],9'b0};
				end
				else
				begin
					data<=addr[15:0]+1;
					addr<=addr+1;
					state<=4'hd;
				end
				we_n<=1;
			end
		endcase	
	end
else
begin
	case (state)
		4'h0:
		begin
			we_n<=0;
			data<=16'h60;
			state<=4'h01;
		end
		4'h01:
		begin
			we_n<=1;
			state<=4'h02;
		end
		4'h02:
		begin
			we_n<=0;
			data<=16'hD0;
			state<=4'h03;
		end
		4'h03:
		begin
			unlock<=1;
			we_n<=1;
			state<=4'h0;
		end
	endcase
end
end
endmodule
