////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.28xd
//  \   \         Application: netgen
//  /   /         Filename: FlashBufferedProgram_top_synthesis.v
// /___/   /\     Timestamp: Wed May 16 09:35:23 2018
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -intstyle ise -insert_glbl true -w -dir netgen/synthesis -ofmt verilog -sim FlashBufferedProgram_top.ngc FlashBufferedProgram_top_synthesis.v 
// Device	: xc7k325t-1-ffg676
// Input file	: FlashBufferedProgram_top.ngc
// Output file	: C:\Users\User\FlashBufferedProgram\netgen\synthesis\FlashBufferedProgram_top_synthesis.v
// # of Modules	: 1
// Design Name	: FlashBufferedProgram_top
// Xilinx        : C:\Xilinx\14.2\ISE_DS\ISE\
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module FlashBufferedProgram_top (
  system_clk_p, system_clk_n, bpi_we_n, bpi_ce_n, bpi_oe_n, bpi_adv, led_0, led_1, led_2, led_3, bpi_addr_cmd, bpi_data
);
  input system_clk_p;
  input system_clk_n;
  output bpi_we_n;
  output bpi_ce_n;
  output bpi_oe_n;
  output bpi_adv;
  output led_0;
  output led_1;
  output led_2;
  output led_3;
  output [25 : 0] bpi_addr_cmd;
  inout [15 : 0] bpi_data;
  wire clk;
  wire led_19;
  wire unlock_20;
  wire GND_1_o_clk_DFF_29_21;
  wire we_n_22;
  wire ce_n_23;
  wire oe_n_24;
  wire adv_28;
  wire state_FSM_FFd1_29;
  wire state_FSM_FFd2_30;
  wire state_FSM_FFd4_31;
  wire state_FSM_FFd3_32;
  wire GND_1_o_clk_DFF_21_inv;
  wire \clkgen/clkfbout_buf ;
  wire \clkgen/clkout0 ;
  wire \clkgen/clkfbout ;
  wire \clkgen/clkin1 ;
  wire \state_FSM_FFd4-In ;
  wire \state_FSM_FFd3-In ;
  wire \state_FSM_FFd2-In ;
  wire \state_FSM_FFd1-In ;
  wire \Madd_addr[15]_GND_1_o_add_9_OUT_lut<0> ;
  wire \Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ;
  wire N0;
  wire N1;
  wire N2;
  wire N3;
  wire N4;
  wire N5;
  wire N6;
  wire N7;
  wire N8;
  wire N9;
  wire N10;
  wire N11;
  wire N12;
  wire N13;
  wire N14;
  wire N15;
  wire GND_1_o_clk_DFF_29_rstpot_126;
  wire ce_n_rstpot_127;
  wire oe_n_rstpot_128;
  wire adv_rstpot_129;
  wire we_n_rstpot_130;
  wire led_rstpot_131;
  wire unlock_rstpot_132;
  wire _n0388_inv1_cepot;
  wire status_1_dpot_134;
  wire status_2_dpot_135;
  wire status_3_dpot_136;
  wire state_FSM_FFd4_1_137;
  wire \NLW_clkgen/mmcm_adv_inst_CLKOUT3_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_CLKOUT3B_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_CLKFBOUTB_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_CLKOUT1_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_CLKOUT5_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DRDY_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_CLKOUT4_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_CLKOUT1B_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_CLKOUT0B_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_CLKOUT2_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_CLKOUT2B_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_PSDONE_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_CLKOUT6_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_LOCKED_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DO<15>_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DO<14>_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DO<13>_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DO<12>_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DO<11>_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DO<10>_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DO<9>_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DO<8>_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DO<7>_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DO<6>_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DO<5>_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DO<4>_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DO<3>_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DO<2>_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DO<1>_UNCONNECTED ;
  wire \NLW_clkgen/mmcm_adv_inst_DO<0>_UNCONNECTED ;
  wire [15 : 0] data;
  wire [3 : 1] status;
  wire [15 : 0] n0159;
  VCC   XST_VCC (
    .P(\Madd_addr[15]_GND_1_o_add_9_OUT_lut<0> )
  );
  GND   XST_GND (
    .G(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> )
  );
  FDE   status_1 (
    .C(clk),
    .CE(_n0388_inv1_cepot),
    .D(status_1_dpot_134),
    .Q(status[1])
  );
  FDE   status_2 (
    .C(clk),
    .CE(_n0388_inv1_cepot),
    .D(status_2_dpot_135),
    .Q(status[2])
  );
  FDE   status_3 (
    .C(clk),
    .CE(_n0388_inv1_cepot),
    .D(status_3_dpot_136),
    .Q(status[3])
  );
  FD   data_0 (
    .C(clk),
    .D(n0159[0]),
    .Q(data[0])
  );
  FD   data_1 (
    .C(clk),
    .D(n0159[1]),
    .Q(data[1])
  );
  FD   data_2 (
    .C(clk),
    .D(n0159[2]),
    .Q(data[2])
  );
  FD   data_3 (
    .C(clk),
    .D(n0159[3]),
    .Q(data[3])
  );
  FD   data_4 (
    .C(clk),
    .D(n0159[4]),
    .Q(data[4])
  );
  FD   data_5 (
    .C(clk),
    .D(n0159[5]),
    .Q(data[5])
  );
  FD   data_6 (
    .C(clk),
    .D(n0159[6]),
    .Q(data[6])
  );
  FD   data_7 (
    .C(clk),
    .D(n0159[7]),
    .Q(data[7])
  );
  FD   data_8 (
    .C(clk),
    .D(n0159[8]),
    .Q(data[8])
  );
  FD   data_9 (
    .C(clk),
    .D(n0159[9]),
    .Q(data[9])
  );
  FD   data_10 (
    .C(clk),
    .D(n0159[10]),
    .Q(data[10])
  );
  FD   data_11 (
    .C(clk),
    .D(n0159[11]),
    .Q(data[11])
  );
  FD   data_12 (
    .C(clk),
    .D(n0159[12]),
    .Q(data[12])
  );
  FD   data_13 (
    .C(clk),
    .D(n0159[13]),
    .Q(data[13])
  );
  FD   data_14 (
    .C(clk),
    .D(n0159[14]),
    .Q(data[14])
  );
  FD   data_15 (
    .C(clk),
    .D(n0159[15]),
    .Q(data[15])
  );
  FD #(
    .INIT ( 1'b0 ))
  state_FSM_FFd4 (
    .C(clk),
    .D(\state_FSM_FFd4-In ),
    .Q(state_FSM_FFd4_31)
  );
  FD #(
    .INIT ( 1'b0 ))
  state_FSM_FFd3 (
    .C(clk),
    .D(\state_FSM_FFd3-In ),
    .Q(state_FSM_FFd3_32)
  );
  FD #(
    .INIT ( 1'b0 ))
  state_FSM_FFd2 (
    .C(clk),
    .D(\state_FSM_FFd2-In ),
    .Q(state_FSM_FFd2_30)
  );
  FD #(
    .INIT ( 1'b0 ))
  state_FSM_FFd1 (
    .C(clk),
    .D(\state_FSM_FFd1-In ),
    .Q(state_FSM_FFd1_29)
  );
  BUFG   \clkgen/clkout1_buf  (
    .O(clk),
    .I(\clkgen/clkout0 )
  );
  BUFG   \clkgen/clkf_buf  (
    .O(\clkgen/clkfbout_buf ),
    .I(\clkgen/clkfbout )
  );
  MMCME2_ADV #(
    .BANDWIDTH ( "OPTIMIZED" ),
    .CLKFBOUT_USE_FINE_PS ( "FALSE" ),
    .CLKOUT0_USE_FINE_PS ( "FALSE" ),
    .CLKOUT1_USE_FINE_PS ( "FALSE" ),
    .CLKOUT2_USE_FINE_PS ( "FALSE" ),
    .CLKOUT3_USE_FINE_PS ( "FALSE" ),
    .CLKOUT4_CASCADE ( "FALSE" ),
    .CLKOUT4_USE_FINE_PS ( "FALSE" ),
    .CLKOUT5_USE_FINE_PS ( "FALSE" ),
    .CLKOUT6_USE_FINE_PS ( "FALSE" ),
    .COMPENSATION ( "ZHOLD" ),
    .SS_EN ( "FALSE" ),
    .SS_MODE ( "CENTER_HIGH" ),
    .STARTUP_WAIT ( "FALSE" ),
    .CLKOUT1_DIVIDE ( 1 ),
    .CLKOUT2_DIVIDE ( 1 ),
    .CLKOUT3_DIVIDE ( 1 ),
    .CLKOUT4_DIVIDE ( 1 ),
    .CLKOUT5_DIVIDE ( 1 ),
    .CLKOUT6_DIVIDE ( 1 ),
    .DIVCLK_DIVIDE ( 5 ),
    .SS_MOD_PERIOD ( 10000 ),
    .CLKFBOUT_MULT_F ( 16.000000 ),
    .CLKFBOUT_PHASE ( 0.000000 ),
    .CLKIN1_PERIOD ( 5.000000 ),
    .CLKIN2_PERIOD ( 0.000000 ),
    .CLKOUT0_DIVIDE_F ( 128.000000 ),
    .CLKOUT0_DUTY_CYCLE ( 0.500000 ),
    .CLKOUT0_PHASE ( 0.000000 ),
    .CLKOUT1_DUTY_CYCLE ( 0.500000 ),
    .CLKOUT1_PHASE ( 0.000000 ),
    .CLKOUT2_DUTY_CYCLE ( 0.500000 ),
    .CLKOUT2_PHASE ( 0.000000 ),
    .CLKOUT3_DUTY_CYCLE ( 0.500000 ),
    .CLKOUT3_PHASE ( 0.000000 ),
    .CLKOUT4_DUTY_CYCLE ( 0.500000 ),
    .CLKOUT4_PHASE ( 0.000000 ),
    .CLKOUT5_DUTY_CYCLE ( 0.500000 ),
    .CLKOUT5_PHASE ( 0.000000 ),
    .CLKOUT6_DUTY_CYCLE ( 0.500000 ),
    .CLKOUT6_PHASE ( 0.000000 ),
    .REF_JITTER1 ( 0.010000 ),
    .REF_JITTER2 ( 0.010000 ))
  \clkgen/mmcm_adv_inst  (
    .CLKOUT3(\NLW_clkgen/mmcm_adv_inst_CLKOUT3_UNCONNECTED ),
    .CLKFBIN(\clkgen/clkfbout_buf ),
    .PSCLK(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .CLKOUT3B(\NLW_clkgen/mmcm_adv_inst_CLKOUT3B_UNCONNECTED ),
    .PWRDWN(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .DCLK(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .CLKFBOUT(\clkgen/clkfbout ),
    .CLKFBSTOPPED(\NLW_clkgen/mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED ),
    .CLKFBOUTB(\NLW_clkgen/mmcm_adv_inst_CLKFBOUTB_UNCONNECTED ),
    .CLKOUT1(\NLW_clkgen/mmcm_adv_inst_CLKOUT1_UNCONNECTED ),
    .DEN(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .CLKOUT5(\NLW_clkgen/mmcm_adv_inst_CLKOUT5_UNCONNECTED ),
    .CLKINSEL(\Madd_addr[15]_GND_1_o_add_9_OUT_lut<0> ),
    .CLKIN2(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .DRDY(\NLW_clkgen/mmcm_adv_inst_DRDY_UNCONNECTED ),
    .RST(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .PSINCDEC(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .DWE(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .PSEN(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .CLKOUT0(\clkgen/clkout0 ),
    .CLKOUT4(\NLW_clkgen/mmcm_adv_inst_CLKOUT4_UNCONNECTED ),
    .CLKOUT1B(\NLW_clkgen/mmcm_adv_inst_CLKOUT1B_UNCONNECTED ),
    .CLKINSTOPPED(\NLW_clkgen/mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED ),
    .CLKOUT0B(\NLW_clkgen/mmcm_adv_inst_CLKOUT0B_UNCONNECTED ),
    .CLKIN1(\clkgen/clkin1 ),
    .CLKOUT2(\NLW_clkgen/mmcm_adv_inst_CLKOUT2_UNCONNECTED ),
    .CLKOUT2B(\NLW_clkgen/mmcm_adv_inst_CLKOUT2B_UNCONNECTED ),
    .PSDONE(\NLW_clkgen/mmcm_adv_inst_PSDONE_UNCONNECTED ),
    .CLKOUT6(\NLW_clkgen/mmcm_adv_inst_CLKOUT6_UNCONNECTED ),
    .LOCKED(\NLW_clkgen/mmcm_adv_inst_LOCKED_UNCONNECTED ),
    .DI({\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , \Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , \Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , 
\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , \Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , \Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , 
\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , \Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , \Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , 
\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , \Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , \Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , 
\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , \Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , \Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , 
\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> }),
    .DO({\NLW_clkgen/mmcm_adv_inst_DO<15>_UNCONNECTED , \NLW_clkgen/mmcm_adv_inst_DO<14>_UNCONNECTED , \NLW_clkgen/mmcm_adv_inst_DO<13>_UNCONNECTED , 
\NLW_clkgen/mmcm_adv_inst_DO<12>_UNCONNECTED , \NLW_clkgen/mmcm_adv_inst_DO<11>_UNCONNECTED , \NLW_clkgen/mmcm_adv_inst_DO<10>_UNCONNECTED , 
\NLW_clkgen/mmcm_adv_inst_DO<9>_UNCONNECTED , \NLW_clkgen/mmcm_adv_inst_DO<8>_UNCONNECTED , \NLW_clkgen/mmcm_adv_inst_DO<7>_UNCONNECTED , 
\NLW_clkgen/mmcm_adv_inst_DO<6>_UNCONNECTED , \NLW_clkgen/mmcm_adv_inst_DO<5>_UNCONNECTED , \NLW_clkgen/mmcm_adv_inst_DO<4>_UNCONNECTED , 
\NLW_clkgen/mmcm_adv_inst_DO<3>_UNCONNECTED , \NLW_clkgen/mmcm_adv_inst_DO<2>_UNCONNECTED , \NLW_clkgen/mmcm_adv_inst_DO<1>_UNCONNECTED , 
\NLW_clkgen/mmcm_adv_inst_DO<0>_UNCONNECTED }),
    .DADDR({\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , \Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , \Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , 
\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , \Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , \Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> , 
\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> })
  );
  IBUFGDS #(
    .CAPACITANCE ( "DONT_CARE" ),
    .DIFF_TERM ( "FALSE" ),
    .IBUF_DELAY_VALUE ( "0" ),
    .IBUF_LOW_PWR ( "TRUE" ),
    .IOSTANDARD ( "DEFAULT" ))
  \clkgen/clkin1_buf  (
    .I(system_clk_p),
    .IB(system_clk_n),
    .O(\clkgen/clkin1 )
  );
  LUT6 #(
    .INIT ( 64'hFFAAAAAA2264AAAA ))
  \state_FSM_FFd1-In1  (
    .I0(state_FSM_FFd1_29),
    .I1(state_FSM_FFd3_32),
    .I2(N8),
    .I3(state_FSM_FFd4_31),
    .I4(unlock_20),
    .I5(state_FSM_FFd2_30),
    .O(\state_FSM_FFd1-In )
  );
  LUT6 #(
    .INIT ( 64'hBFAFBFAF80508058 ))
  \state_FSM_FFd3-In1  (
    .I0(state_FSM_FFd1_29),
    .I1(unlock_20),
    .I2(state_FSM_FFd4_31),
    .I3(state_FSM_FFd2_30),
    .I4(N8),
    .I5(state_FSM_FFd3_32),
    .O(\state_FSM_FFd3-In )
  );
  LUT6 #(
    .INIT ( 64'hB7AAB7EA445D4455 ))
  \state_FSM_FFd4-In1  (
    .I0(state_FSM_FFd1_29),
    .I1(unlock_20),
    .I2(state_FSM_FFd3_32),
    .I3(state_FSM_FFd2_30),
    .I4(N8),
    .I5(state_FSM_FFd4_31),
    .O(\state_FSM_FFd4-In )
  );
  LUT6 #(
    .INIT ( 64'hBA6AAA2AAA6AAA6A ))
  \state_FSM_FFd2-In1  (
    .I0(state_FSM_FFd2_30),
    .I1(state_FSM_FFd1_29),
    .I2(unlock_20),
    .I3(state_FSM_FFd4_31),
    .I4(state_FSM_FFd3_32),
    .I5(N8),
    .O(\state_FSM_FFd2-In )
  );
  IOBUF   bpi_data_15_IOBUF (
    .I(data[15]),
    .T(GND_1_o_clk_DFF_21_inv),
    .O(N0),
    .IO(bpi_data[15])
  );
  IOBUF   bpi_data_14_IOBUF (
    .I(data[14]),
    .T(GND_1_o_clk_DFF_21_inv),
    .O(N1),
    .IO(bpi_data[14])
  );
  IOBUF   bpi_data_13_IOBUF (
    .I(data[13]),
    .T(GND_1_o_clk_DFF_21_inv),
    .O(N2),
    .IO(bpi_data[13])
  );
  IOBUF   bpi_data_12_IOBUF (
    .I(data[12]),
    .T(GND_1_o_clk_DFF_21_inv),
    .O(N3),
    .IO(bpi_data[12])
  );
  IOBUF   bpi_data_11_IOBUF (
    .I(data[11]),
    .T(GND_1_o_clk_DFF_21_inv),
    .O(N4),
    .IO(bpi_data[11])
  );
  IOBUF   bpi_data_10_IOBUF (
    .I(data[10]),
    .T(GND_1_o_clk_DFF_21_inv),
    .O(N5),
    .IO(bpi_data[10])
  );
  IOBUF   bpi_data_9_IOBUF (
    .I(data[9]),
    .T(GND_1_o_clk_DFF_21_inv),
    .O(N6),
    .IO(bpi_data[9])
  );
  IOBUF   bpi_data_8_IOBUF (
    .I(data[8]),
    .T(GND_1_o_clk_DFF_21_inv),
    .O(N7),
    .IO(bpi_data[8])
  );
  IOBUF   bpi_data_7_IOBUF (
    .I(data[7]),
    .T(GND_1_o_clk_DFF_21_inv),
    .O(N8),
    .IO(bpi_data[7])
  );
  IOBUF   bpi_data_6_IOBUF (
    .I(data[6]),
    .T(GND_1_o_clk_DFF_21_inv),
    .O(N9),
    .IO(bpi_data[6])
  );
  IOBUF   bpi_data_5_IOBUF (
    .I(data[5]),
    .T(GND_1_o_clk_DFF_21_inv),
    .O(N10),
    .IO(bpi_data[5])
  );
  IOBUF   bpi_data_4_IOBUF (
    .I(data[4]),
    .T(GND_1_o_clk_DFF_21_inv),
    .O(N11),
    .IO(bpi_data[4])
  );
  IOBUF   bpi_data_3_IOBUF (
    .I(data[3]),
    .T(GND_1_o_clk_DFF_21_inv),
    .O(N12),
    .IO(bpi_data[3])
  );
  IOBUF   bpi_data_2_IOBUF (
    .I(data[2]),
    .T(GND_1_o_clk_DFF_21_inv),
    .O(N13),
    .IO(bpi_data[2])
  );
  IOBUF   bpi_data_1_IOBUF (
    .I(data[1]),
    .T(GND_1_o_clk_DFF_21_inv),
    .O(N14),
    .IO(bpi_data[1])
  );
  IOBUF   bpi_data_0_IOBUF (
    .I(data[0]),
    .T(GND_1_o_clk_DFF_21_inv),
    .O(N15),
    .IO(bpi_data[0])
  );
  OBUF   bpi_addr_cmd_25_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[25])
  );
  OBUF   bpi_addr_cmd_24_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[24])
  );
  OBUF   bpi_addr_cmd_23_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[23])
  );
  OBUF   bpi_addr_cmd_22_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[22])
  );
  OBUF   bpi_addr_cmd_21_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[21])
  );
  OBUF   bpi_addr_cmd_20_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[20])
  );
  OBUF   bpi_addr_cmd_19_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[19])
  );
  OBUF   bpi_addr_cmd_18_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[18])
  );
  OBUF   bpi_addr_cmd_17_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[17])
  );
  OBUF   bpi_addr_cmd_16_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_lut<0> ),
    .O(bpi_addr_cmd[16])
  );
  OBUF   bpi_addr_cmd_15_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[15])
  );
  OBUF   bpi_addr_cmd_14_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[14])
  );
  OBUF   bpi_addr_cmd_13_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[13])
  );
  OBUF   bpi_addr_cmd_12_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[12])
  );
  OBUF   bpi_addr_cmd_11_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[11])
  );
  OBUF   bpi_addr_cmd_10_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[10])
  );
  OBUF   bpi_addr_cmd_9_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[9])
  );
  OBUF   bpi_addr_cmd_8_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[8])
  );
  OBUF   bpi_addr_cmd_7_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[7])
  );
  OBUF   bpi_addr_cmd_6_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[6])
  );
  OBUF   bpi_addr_cmd_5_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[5])
  );
  OBUF   bpi_addr_cmd_4_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[4])
  );
  OBUF   bpi_addr_cmd_3_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[3])
  );
  OBUF   bpi_addr_cmd_2_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[2])
  );
  OBUF   bpi_addr_cmd_1_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[1])
  );
  OBUF   bpi_addr_cmd_0_OBUF (
    .I(\Madd_addr[15]_GND_1_o_add_9_OUT_cy<0> ),
    .O(bpi_addr_cmd[0])
  );
  OBUF   bpi_we_n_OBUF (
    .I(we_n_22),
    .O(bpi_we_n)
  );
  OBUF   bpi_ce_n_OBUF (
    .I(ce_n_23),
    .O(bpi_ce_n)
  );
  OBUF   bpi_oe_n_OBUF (
    .I(oe_n_24),
    .O(bpi_oe_n)
  );
  OBUF   bpi_adv_OBUF (
    .I(adv_28),
    .O(bpi_adv)
  );
  OBUF   led_0_OBUF (
    .I(led_19),
    .O(led_0)
  );
  OBUF   led_1_OBUF (
    .I(status[1]),
    .O(led_1)
  );
  OBUF   led_2_OBUF (
    .I(status[2]),
    .O(led_2)
  );
  OBUF   led_3_OBUF (
    .I(status[3]),
    .O(led_3)
  );
  FD   GND_1_o_clk_DFF_29 (
    .C(clk),
    .D(GND_1_o_clk_DFF_29_rstpot_126),
    .Q(GND_1_o_clk_DFF_29_21)
  );
  FD #(
    .INIT ( 1'b1 ))
  ce_n (
    .C(clk),
    .D(ce_n_rstpot_127),
    .Q(ce_n_23)
  );
  FD #(
    .INIT ( 1'b1 ))
  oe_n (
    .C(clk),
    .D(oe_n_rstpot_128),
    .Q(oe_n_24)
  );
  FD #(
    .INIT ( 1'b0 ))
  adv (
    .C(clk),
    .D(adv_rstpot_129),
    .Q(adv_28)
  );
  FD #(
    .INIT ( 1'b1 ))
  we_n (
    .C(clk),
    .D(we_n_rstpot_130),
    .Q(we_n_22)
  );
  LUT6 #(
    .INIT ( 64'h2AAAAABA0A8AA8BB ))
  Mmux_n0159121 (
    .I0(N10),
    .I1(state_FSM_FFd1_29),
    .I2(unlock_20),
    .I3(state_FSM_FFd3_32),
    .I4(state_FSM_FFd2_30),
    .I5(state_FSM_FFd4_31),
    .O(n0159[5])
  );
  LUT6 #(
    .INIT ( 64'hEAAACAABABAA8BA8 ))
  Mmux_n0159141 (
    .I0(N8),
    .I1(state_FSM_FFd1_29),
    .I2(state_FSM_FFd2_30),
    .I3(unlock_20),
    .I4(state_FSM_FFd4_31),
    .I5(state_FSM_FFd3_32),
    .O(n0159[7])
  );
  LUT6 #(
    .INIT ( 64'hEAEAAA2BAAAA8A08 ))
  Mmux_n0159111 (
    .I0(N11),
    .I1(state_FSM_FFd2_30),
    .I2(unlock_20),
    .I3(state_FSM_FFd4_31),
    .I4(state_FSM_FFd1_29),
    .I5(state_FSM_FFd3_32),
    .O(n0159[4])
  );
  LUT6 #(
    .INIT ( 64'h2AAA0A8AAABAA8B8 ))
  Mmux_n0159101 (
    .I0(N12),
    .I1(state_FSM_FFd1_29),
    .I2(unlock_20),
    .I3(state_FSM_FFd3_32),
    .I4(state_FSM_FFd4_31),
    .I5(state_FSM_FFd2_30),
    .O(n0159[3])
  );
  LUT6 #(
    .INIT ( 64'hEAAAEA2BAAAEAA2F ))
  Mmux_n0159131 (
    .I0(N9),
    .I1(unlock_20),
    .I2(state_FSM_FFd2_30),
    .I3(state_FSM_FFd1_29),
    .I4(state_FSM_FFd4_31),
    .I5(state_FSM_FFd3_32),
    .O(n0159[6])
  );
  LUT6 #(
    .INIT ( 64'h2A2AAA28AAAAA220 ))
  Mmux_n015911 (
    .I0(N15),
    .I1(unlock_20),
    .I2(state_FSM_FFd2_30),
    .I3(state_FSM_FFd4_31),
    .I4(state_FSM_FFd1_29),
    .I5(state_FSM_FFd3_32),
    .O(n0159[0])
  );
  LUT6 #(
    .INIT ( 64'h2A2AAA28AAAAA220 ))
  Mmux_n015921 (
    .I0(N5),
    .I1(unlock_20),
    .I2(state_FSM_FFd2_30),
    .I3(state_FSM_FFd4_31),
    .I4(state_FSM_FFd1_29),
    .I5(state_FSM_FFd3_32),
    .O(n0159[10])
  );
  LUT6 #(
    .INIT ( 64'h2A2AAA28AAAAA220 ))
  Mmux_n015931 (
    .I0(N4),
    .I1(unlock_20),
    .I2(state_FSM_FFd2_30),
    .I3(state_FSM_FFd4_31),
    .I4(state_FSM_FFd1_29),
    .I5(state_FSM_FFd3_32),
    .O(n0159[11])
  );
  LUT6 #(
    .INIT ( 64'h2A2AAA28AAAAA220 ))
  Mmux_n015941 (
    .I0(N3),
    .I1(unlock_20),
    .I2(state_FSM_FFd2_30),
    .I3(state_FSM_FFd4_31),
    .I4(state_FSM_FFd1_29),
    .I5(state_FSM_FFd3_32),
    .O(n0159[12])
  );
  LUT6 #(
    .INIT ( 64'h2A2AAA28AAAAA220 ))
  Mmux_n015951 (
    .I0(N2),
    .I1(unlock_20),
    .I2(state_FSM_FFd2_30),
    .I3(state_FSM_FFd4_31),
    .I4(state_FSM_FFd1_29),
    .I5(state_FSM_FFd3_32),
    .O(n0159[13])
  );
  LUT6 #(
    .INIT ( 64'h2A2AAA28AAAAA220 ))
  Mmux_n015961 (
    .I0(N1),
    .I1(unlock_20),
    .I2(state_FSM_FFd2_30),
    .I3(state_FSM_FFd4_31),
    .I4(state_FSM_FFd1_29),
    .I5(state_FSM_FFd3_32),
    .O(n0159[14])
  );
  LUT6 #(
    .INIT ( 64'h2A2AAA28AAAAA220 ))
  Mmux_n015971 (
    .I0(N0),
    .I1(unlock_20),
    .I2(state_FSM_FFd2_30),
    .I3(state_FSM_FFd4_31),
    .I4(state_FSM_FFd1_29),
    .I5(state_FSM_FFd3_32),
    .O(n0159[15])
  );
  LUT6 #(
    .INIT ( 64'h2A2AAA28AAAAA220 ))
  Mmux_n015981 (
    .I0(N14),
    .I1(unlock_20),
    .I2(state_FSM_FFd2_30),
    .I3(state_FSM_FFd4_31),
    .I4(state_FSM_FFd1_29),
    .I5(state_FSM_FFd3_32),
    .O(n0159[1])
  );
  LUT6 #(
    .INIT ( 64'h2A2AAA28AAAAA220 ))
  Mmux_n015991 (
    .I0(N13),
    .I1(unlock_20),
    .I2(state_FSM_FFd2_30),
    .I3(state_FSM_FFd4_31),
    .I4(state_FSM_FFd1_29),
    .I5(state_FSM_FFd3_32),
    .O(n0159[2])
  );
  LUT6 #(
    .INIT ( 64'h2A2AAA28AAAAA220 ))
  Mmux_n0159151 (
    .I0(N7),
    .I1(unlock_20),
    .I2(state_FSM_FFd2_30),
    .I3(state_FSM_FFd4_31),
    .I4(state_FSM_FFd1_29),
    .I5(state_FSM_FFd3_32),
    .O(n0159[8])
  );
  LUT6 #(
    .INIT ( 64'h2A2AAA28AAAAA220 ))
  Mmux_n0159161 (
    .I0(N6),
    .I1(unlock_20),
    .I2(state_FSM_FFd2_30),
    .I3(state_FSM_FFd4_31),
    .I4(state_FSM_FFd1_29),
    .I5(state_FSM_FFd3_32),
    .O(n0159[9])
  );
  FD #(
    .INIT ( 1'b0 ))
  led (
    .C(clk),
    .D(led_rstpot_131),
    .Q(led_19)
  );
  FD #(
    .INIT ( 1'b0 ))
  unlock (
    .C(clk),
    .D(unlock_rstpot_132),
    .Q(unlock_20)
  );
  LUT6 #(
    .INIT ( 64'hAAABE2E82AEFAA20 ))
  we_n_rstpot (
    .I0(we_n_22),
    .I1(unlock_20),
    .I2(state_FSM_FFd2_30),
    .I3(state_FSM_FFd1_29),
    .I4(state_FSM_FFd4_31),
    .I5(state_FSM_FFd3_32),
    .O(we_n_rstpot_130)
  );
  LUT6 #(
    .INIT ( 64'hFAAA8AB3AAAAABAB ))
  GND_1_o_clk_DFF_29_rstpot (
    .I0(GND_1_o_clk_DFF_29_21),
    .I1(state_FSM_FFd4_31),
    .I2(state_FSM_FFd2_30),
    .I3(state_FSM_FFd3_32),
    .I4(state_FSM_FFd1_29),
    .I5(unlock_20),
    .O(GND_1_o_clk_DFF_29_rstpot_126)
  );
  LUT6 #(
    .INIT ( 64'hAAAAB82BAAAAAAAA ))
  adv_rstpot (
    .I0(adv_28),
    .I1(state_FSM_FFd3_32),
    .I2(state_FSM_FFd2_30),
    .I3(state_FSM_FFd4_31),
    .I4(state_FSM_FFd1_29),
    .I5(unlock_20),
    .O(adv_rstpot_129)
  );
  LUT6 #(
    .INIT ( 64'hAAABAAAAB82AAAAA ))
  oe_n_rstpot (
    .I0(oe_n_24),
    .I1(state_FSM_FFd2_30),
    .I2(state_FSM_FFd3_32),
    .I3(state_FSM_FFd4_31),
    .I4(unlock_20),
    .I5(state_FSM_FFd1_29),
    .O(oe_n_rstpot_128)
  );
  LUT6 #(
    .INIT ( 64'hABAAAAA8AAAAAAAA ))
  ce_n_rstpot (
    .I0(ce_n_23),
    .I1(state_FSM_FFd3_32),
    .I2(state_FSM_FFd2_30),
    .I3(state_FSM_FFd1_29),
    .I4(state_FSM_FFd4_31),
    .I5(unlock_20),
    .O(ce_n_rstpot_127)
  );
  LUT5 #(
    .INIT ( 32'hFFFF1000 ))
  unlock_rstpot (
    .I0(state_FSM_FFd2_30),
    .I1(state_FSM_FFd1_29),
    .I2(state_FSM_FFd3_32),
    .I3(state_FSM_FFd4_31),
    .I4(unlock_20),
    .O(unlock_rstpot_132)
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFF00000800 ))
  led_rstpot (
    .I0(state_FSM_FFd4_31),
    .I1(state_FSM_FFd1_29),
    .I2(state_FSM_FFd2_30),
    .I3(unlock_20),
    .I4(state_FSM_FFd3_32),
    .I5(led_19),
    .O(led_rstpot_131)
  );
  LUT6 #(
    .INIT ( 64'hCCCCCCCCCACCCCCC ))
  status_1_dpot (
    .I0(N10),
    .I1(status[1]),
    .I2(state_FSM_FFd2_30),
    .I3(unlock_20),
    .I4(state_FSM_FFd1_29),
    .I5(state_FSM_FFd3_32),
    .O(status_1_dpot_134)
  );
  LUT6 #(
    .INIT ( 64'hCCCCCCCCCACCCCCC ))
  status_2_dpot (
    .I0(N9),
    .I1(status[2]),
    .I2(state_FSM_FFd2_30),
    .I3(unlock_20),
    .I4(state_FSM_FFd1_29),
    .I5(state_FSM_FFd3_32),
    .O(status_2_dpot_135)
  );
  LUT6 #(
    .INIT ( 64'hCCCCCCCCCACCCCCC ))
  status_3_dpot (
    .I0(N8),
    .I1(status[3]),
    .I2(state_FSM_FFd2_30),
    .I3(unlock_20),
    .I4(state_FSM_FFd1_29),
    .I5(state_FSM_FFd3_32),
    .O(status_3_dpot_136)
  );
  FD #(
    .INIT ( 1'b0 ))
  state_FSM_FFd4_1 (
    .C(clk),
    .D(\state_FSM_FFd4-In ),
    .Q(state_FSM_FFd4_1_137)
  );
  INV   GND_1_o_clk_DFF_21_inv1_INV_0 (
    .I(GND_1_o_clk_DFF_29_21),
    .O(GND_1_o_clk_DFF_21_inv)
  );
  INV   _n0388_inv1_cepot_INV_0 (
    .I(state_FSM_FFd4_1_137),
    .O(_n0388_inv1_cepot)
  );
endmodule


`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

